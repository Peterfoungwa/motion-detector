#!/usr/bin/python
import datetime
from astral import Astral
import pytz

region_name = 'Europe'
city_name = 'Berlin'

tz_BE = pytz.timezone('%s/%s' % (region_name, city_name))

a = Astral()
a.solar_depression = 'civil'

city = a[city_name]

print ('Information for %s/%s\n' % (city_name, city.region))

timezone = city.timezone
print('Timezone: %s' % timezone)

print('Latitude: %.02f; Longitude: %.02f\n' % (city.latitude, city.longitude))

sun = city.sun(date = datetime.date.today(), local = True)
print('Dawn:    %s' % str(sun['dawn']))
print('Sunrise: %s' % str(sun['sunrise']))
print('Noon:    %s' % str(sun['noon']))
print('Sunset:  %s' % str(sun['sunset']))
print('Dusk:    %s' % str(sun['dusk']))

current_time = datetime.datetime.now(tz_BE).strftime('%Y-%m-%d %H:%M:%S')
print (current_time)
sunrise_time = str(sun['sunrise'])
new_sunrise_time = sunrise_time[:sunrise_time.rfind("+")]
print (new_sunrise_time)

if (current_time > sunrise_time):
    #Licht An
else:
    #Do nothing
